namespace MySCADA
{
    public static class Program
    {
        public static SCADA Root = new SCADA();
        public static Main Main_Page = new Main();

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Device PLC_1 = new Device("192.168.1.201");
            Root.AddDevice(PLC_1);

            Motor Motor_1 = new Motor("Motor_1", 500);
            Tag Speed = new Tag("Speed", "");
            Tag Current = new Tag("Current", "");
            Tag Voltage = new Tag("Voltage", "");

            Motor_1.AddTag(Speed);
            Motor_1.AddTag(Current);
            Motor_1.AddTag(Voltage);

            Motor Motor_2 = new Motor("Motor_2", 500);
            Speed = new Tag("Speed", "");
            Current = new Tag("Current", "");
            Voltage = new Tag("Voltage", "");

            Motor_2.AddTag(Speed);
            Motor_2.AddTag(Current);
            Motor_2.AddTag(Voltage);

            Motor Motor_3 = new Motor("Motor_3", 500);
            Speed = new Tag("Speed", "");
            Current = new Tag("Current", "");
            Voltage = new Tag("Voltage", "");

            Motor_3.AddTag(Speed);
            Motor_3.AddTag(Current);
            Motor_3.AddTag(Voltage);

            Root.AddMotor(Motor_1);
            Root.AddMotor(Motor_2); 
            Root.AddMotor(Motor_3);

            Root.RunMotor("Motor_1");
            Root.RunMotor("Motor_2");
            Root.RunMotor("Motor_3");

            Application.EnableVisualStyles();
            // ApplicationConfiguration.Initialize();
            Application.Run(Main_Page);
        }
    }
}
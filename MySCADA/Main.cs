
namespace MySCADA
{
    public partial class Main : Form
    {
        private int btBorderRadius = 30;
        private int btBorderWidth = 1;

        public Main()
        {
            InitializeComponent();
            UpdateTimer.Start();
        }

        private void UpdateTimer_Tick(object sender, EventArgs e)
        {
            lbMotor_1_Speed_TimeStamp.Text = Program.Root.FindMotor("Motor_1").FindTag("Speed").TimeStamp.ToString();
            lbMotor_1_Speed_Value.Text = Convert.ToInt32(Program.Root.FindMotor("Motor_1").FindTag("Speed").Value).ToString();
            lbMotor_1_Current_TimeStamp.Text = Program.Root.FindMotor("Motor_1").FindTag("Current").TimeStamp.ToString();
            lbMotor_1_Current_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_1").FindTag("Current").Value).ToString("0.00");
            lbMotor_1_Voltage_TimeStamp.Text = Program.Root.FindMotor("Motor_1").FindTag("Voltage").TimeStamp.ToString();
            lbMotor_1_Voltage_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_1").FindTag("Voltage").Value).ToString("0.00");

            lbMotor_2_Speed_TimeStamp.Text = Program.Root.FindMotor("Motor_2").FindTag("Speed").TimeStamp.ToString();
            lbMotor_2_Speed_Value.Text = Convert.ToInt32(Program.Root.FindMotor("Motor_2").FindTag("Speed").Value).ToString();
            lbMotor_2_Current_TimeStamp.Text = Program.Root.FindMotor("Motor_2").FindTag("Current").TimeStamp.ToString();
            lbMotor_2_Current_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_2").FindTag("Current").Value).ToString("0.00");
            lbMotor_2_Voltage_TimeStamp.Text = Program.Root.FindMotor("Motor_2").FindTag("Voltage").TimeStamp.ToString();
            lbMotor_2_Voltage_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_2").FindTag("Voltage").Value).ToString("0.00");

            lbMotor_3_Speed_TimeStamp.Text = Program.Root.FindMotor("Motor_3").FindTag("Speed").TimeStamp.ToString();
            lbMotor_3_Speed_Value.Text = Convert.ToInt32(Program.Root.FindMotor("Motor_3").FindTag("Speed").Value).ToString();
            lbMotor_3_Current_TimeStamp.Text = Program.Root.FindMotor("Motor_3").FindTag("Current").TimeStamp.ToString();
            lbMotor_3_Current_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_3").FindTag("Current").Value).ToString("0.00");
            lbMotor_3_Voltage_TimeStamp.Text = Program.Root.FindMotor("Motor_3").FindTag("Voltage").TimeStamp.ToString();
            lbMotor_3_Voltage_Value.Text = Convert.ToDouble(Program.Root.FindMotor("Motor_3").FindTag("Voltage").Value).ToString("0.00");
        }

        private void btSleep_Motor_1_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_1").Sleep();
        }

        private void btResume_Motor_1_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_1").Resume();
        }

        private void btKill_Motor_1_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_1").Kill();
        }

        private void btRun_Motor_1_Click(object sender, EventArgs e)
        {
            Program.Root.RunMotor("Motor_1");
        }

        private void btSleep_Motor_2_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_2").Sleep();
        }

        private void btResume_Motor_2_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_2").Resume();
        }

        private void btKill_Motor_2_Click(object sender, EventArgs e)
        {
            Program.Root.FindMotor("Motor_2").Kill();
        }

        private void btRun_Motor_2_Click(object sender, EventArgs e)
        {
            Program.Root.RunMotor("Motor_2");
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }
    }
}
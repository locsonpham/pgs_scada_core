﻿namespace MySCADA
{
    partial class Main
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbMotor_1_Speed = new System.Windows.Forms.Label();
            this.lbMotor_1_Speed_TimeStamp = new System.Windows.Forms.Label();
            this.lbMotor_1_Speed_Value = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbMotor_1_Voltage_Value = new System.Windows.Forms.Label();
            this.lbMotor_1_Voltage_TimeStamp = new System.Windows.Forms.Label();
            this.lbMotor_1_Voltage = new System.Windows.Forms.Label();
            this.lbMotor_1_Current = new System.Windows.Forms.Label();
            this.lbMotor_1_Current_Value = new System.Windows.Forms.Label();
            this.lbMotor_1_Current_TimeStamp = new System.Windows.Forms.Label();
            this.UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbMotor_2_Speed_Value = new System.Windows.Forms.Label();
            this.lbMotor_2_Voltage_Value = new System.Windows.Forms.Label();
            this.lbMotor_2_Speed = new System.Windows.Forms.Label();
            this.lbMotor_2_Voltage_TimeStamp = new System.Windows.Forms.Label();
            this.lbMotor_2_Speed_TimeStamp = new System.Windows.Forms.Label();
            this.lbMotor_2_Voltage = new System.Windows.Forms.Label();
            this.lbMotor_2_Current = new System.Windows.Forms.Label();
            this.lbMotor_2_Current_Value = new System.Windows.Forms.Label();
            this.lbMotor_2_Current_TimeStamp = new System.Windows.Forms.Label();
            this.btSleep_Motor_1 = new System.Windows.Forms.Button();
            this.btKill_Motor_1 = new System.Windows.Forms.Button();
            this.btResume_Motor_1 = new System.Windows.Forms.Button();
            this.btRun_Motor_1 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbMotor_3_Speed_Value = new System.Windows.Forms.Label();
            this.lbMotor_3_Voltage_Value = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbMotor_3_Voltage_TimeStamp = new System.Windows.Forms.Label();
            this.lbMotor_3_Speed_TimeStamp = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbMotor_3_Current_Value = new System.Windows.Forms.Label();
            this.lbMotor_3_Current_TimeStamp = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbMotor_1_Speed
            // 
            this.lbMotor_1_Speed.AutoSize = true;
            this.lbMotor_1_Speed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Speed.Location = new System.Drawing.Point(26, 39);
            this.lbMotor_1_Speed.Name = "lbMotor_1_Speed";
            this.lbMotor_1_Speed.Size = new System.Drawing.Size(51, 20);
            this.lbMotor_1_Speed.TabIndex = 0;
            this.lbMotor_1_Speed.Text = "Speed";
            // 
            // lbMotor_1_Speed_TimeStamp
            // 
            this.lbMotor_1_Speed_TimeStamp.AutoSize = true;
            this.lbMotor_1_Speed_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Speed_TimeStamp.Location = new System.Drawing.Point(111, 39);
            this.lbMotor_1_Speed_TimeStamp.Name = "lbMotor_1_Speed_TimeStamp";
            this.lbMotor_1_Speed_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_1_Speed_TimeStamp.TabIndex = 1;
            this.lbMotor_1_Speed_TimeStamp.Text = "Time";
            // 
            // lbMotor_1_Speed_Value
            // 
            this.lbMotor_1_Speed_Value.AutoSize = true;
            this.lbMotor_1_Speed_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Speed_Value.Location = new System.Drawing.Point(331, 39);
            this.lbMotor_1_Speed_Value.Name = "lbMotor_1_Speed_Value";
            this.lbMotor_1_Speed_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_1_Speed_Value.TabIndex = 2;
            this.lbMotor_1_Speed_Value.Text = "Current";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbMotor_1_Speed_Value);
            this.groupBox1.Controls.Add(this.lbMotor_1_Voltage_Value);
            this.groupBox1.Controls.Add(this.lbMotor_1_Speed);
            this.groupBox1.Controls.Add(this.lbMotor_1_Voltage_TimeStamp);
            this.groupBox1.Controls.Add(this.lbMotor_1_Speed_TimeStamp);
            this.groupBox1.Controls.Add(this.lbMotor_1_Voltage);
            this.groupBox1.Controls.Add(this.lbMotor_1_Current);
            this.groupBox1.Controls.Add(this.lbMotor_1_Current_Value);
            this.groupBox1.Controls.Add(this.lbMotor_1_Current_TimeStamp);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 168);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Motor1";
            // 
            // lbMotor_1_Voltage_Value
            // 
            this.lbMotor_1_Voltage_Value.AutoSize = true;
            this.lbMotor_1_Voltage_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Voltage_Value.Location = new System.Drawing.Point(331, 126);
            this.lbMotor_1_Voltage_Value.Name = "lbMotor_1_Voltage_Value";
            this.lbMotor_1_Voltage_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_1_Voltage_Value.TabIndex = 11;
            this.lbMotor_1_Voltage_Value.Text = "Current";
            // 
            // lbMotor_1_Voltage_TimeStamp
            // 
            this.lbMotor_1_Voltage_TimeStamp.AutoSize = true;
            this.lbMotor_1_Voltage_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Voltage_TimeStamp.Location = new System.Drawing.Point(111, 126);
            this.lbMotor_1_Voltage_TimeStamp.Name = "lbMotor_1_Voltage_TimeStamp";
            this.lbMotor_1_Voltage_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_1_Voltage_TimeStamp.TabIndex = 10;
            this.lbMotor_1_Voltage_TimeStamp.Text = "Time";
            // 
            // lbMotor_1_Voltage
            // 
            this.lbMotor_1_Voltage.AutoSize = true;
            this.lbMotor_1_Voltage.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Voltage.Location = new System.Drawing.Point(26, 126);
            this.lbMotor_1_Voltage.Name = "lbMotor_1_Voltage";
            this.lbMotor_1_Voltage.Size = new System.Drawing.Size(60, 20);
            this.lbMotor_1_Voltage.TabIndex = 9;
            this.lbMotor_1_Voltage.Text = "Voltage";
            // 
            // lbMotor_1_Current
            // 
            this.lbMotor_1_Current.AutoSize = true;
            this.lbMotor_1_Current.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Current.Location = new System.Drawing.Point(26, 83);
            this.lbMotor_1_Current.Name = "lbMotor_1_Current";
            this.lbMotor_1_Current.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_1_Current.TabIndex = 5;
            this.lbMotor_1_Current.Text = "Current";
            // 
            // lbMotor_1_Current_Value
            // 
            this.lbMotor_1_Current_Value.AutoSize = true;
            this.lbMotor_1_Current_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Current_Value.Location = new System.Drawing.Point(331, 83);
            this.lbMotor_1_Current_Value.Name = "lbMotor_1_Current_Value";
            this.lbMotor_1_Current_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_1_Current_Value.TabIndex = 7;
            this.lbMotor_1_Current_Value.Text = "Current";
            // 
            // lbMotor_1_Current_TimeStamp
            // 
            this.lbMotor_1_Current_TimeStamp.AutoSize = true;
            this.lbMotor_1_Current_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_1_Current_TimeStamp.Location = new System.Drawing.Point(111, 83);
            this.lbMotor_1_Current_TimeStamp.Name = "lbMotor_1_Current_TimeStamp";
            this.lbMotor_1_Current_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_1_Current_TimeStamp.TabIndex = 6;
            this.lbMotor_1_Current_TimeStamp.Text = "Time";
            // 
            // UpdateTimer
            // 
            this.UpdateTimer.Interval = 250;
            this.UpdateTimer.Tick += new System.EventHandler(this.UpdateTimer_Tick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbMotor_2_Speed_Value);
            this.groupBox2.Controls.Add(this.lbMotor_2_Voltage_Value);
            this.groupBox2.Controls.Add(this.lbMotor_2_Speed);
            this.groupBox2.Controls.Add(this.lbMotor_2_Voltage_TimeStamp);
            this.groupBox2.Controls.Add(this.lbMotor_2_Speed_TimeStamp);
            this.groupBox2.Controls.Add(this.lbMotor_2_Voltage);
            this.groupBox2.Controls.Add(this.lbMotor_2_Current);
            this.groupBox2.Controls.Add(this.lbMotor_2_Current_Value);
            this.groupBox2.Controls.Add(this.lbMotor_2_Current_TimeStamp);
            this.groupBox2.Location = new System.Drawing.Point(12, 197);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(413, 168);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Motor2";
            // 
            // lbMotor_2_Speed_Value
            // 
            this.lbMotor_2_Speed_Value.AutoSize = true;
            this.lbMotor_2_Speed_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Speed_Value.Location = new System.Drawing.Point(331, 39);
            this.lbMotor_2_Speed_Value.Name = "lbMotor_2_Speed_Value";
            this.lbMotor_2_Speed_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_2_Speed_Value.TabIndex = 2;
            this.lbMotor_2_Speed_Value.Text = "Current";
            // 
            // lbMotor_2_Voltage_Value
            // 
            this.lbMotor_2_Voltage_Value.AutoSize = true;
            this.lbMotor_2_Voltage_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Voltage_Value.Location = new System.Drawing.Point(331, 126);
            this.lbMotor_2_Voltage_Value.Name = "lbMotor_2_Voltage_Value";
            this.lbMotor_2_Voltage_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_2_Voltage_Value.TabIndex = 11;
            this.lbMotor_2_Voltage_Value.Text = "Current";
            // 
            // lbMotor_2_Speed
            // 
            this.lbMotor_2_Speed.AutoSize = true;
            this.lbMotor_2_Speed.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Speed.Location = new System.Drawing.Point(26, 39);
            this.lbMotor_2_Speed.Name = "lbMotor_2_Speed";
            this.lbMotor_2_Speed.Size = new System.Drawing.Size(51, 20);
            this.lbMotor_2_Speed.TabIndex = 0;
            this.lbMotor_2_Speed.Text = "Speed";
            // 
            // lbMotor_2_Voltage_TimeStamp
            // 
            this.lbMotor_2_Voltage_TimeStamp.AutoSize = true;
            this.lbMotor_2_Voltage_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Voltage_TimeStamp.Location = new System.Drawing.Point(111, 126);
            this.lbMotor_2_Voltage_TimeStamp.Name = "lbMotor_2_Voltage_TimeStamp";
            this.lbMotor_2_Voltage_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_2_Voltage_TimeStamp.TabIndex = 10;
            this.lbMotor_2_Voltage_TimeStamp.Text = "Time";
            // 
            // lbMotor_2_Speed_TimeStamp
            // 
            this.lbMotor_2_Speed_TimeStamp.AutoSize = true;
            this.lbMotor_2_Speed_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Speed_TimeStamp.Location = new System.Drawing.Point(111, 39);
            this.lbMotor_2_Speed_TimeStamp.Name = "lbMotor_2_Speed_TimeStamp";
            this.lbMotor_2_Speed_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_2_Speed_TimeStamp.TabIndex = 1;
            this.lbMotor_2_Speed_TimeStamp.Text = "Time";
            // 
            // lbMotor_2_Voltage
            // 
            this.lbMotor_2_Voltage.AutoSize = true;
            this.lbMotor_2_Voltage.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Voltage.Location = new System.Drawing.Point(26, 126);
            this.lbMotor_2_Voltage.Name = "lbMotor_2_Voltage";
            this.lbMotor_2_Voltage.Size = new System.Drawing.Size(60, 20);
            this.lbMotor_2_Voltage.TabIndex = 9;
            this.lbMotor_2_Voltage.Text = "Voltage";
            // 
            // lbMotor_2_Current
            // 
            this.lbMotor_2_Current.AutoSize = true;
            this.lbMotor_2_Current.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Current.Location = new System.Drawing.Point(26, 83);
            this.lbMotor_2_Current.Name = "lbMotor_2_Current";
            this.lbMotor_2_Current.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_2_Current.TabIndex = 5;
            this.lbMotor_2_Current.Text = "Current";
            // 
            // lbMotor_2_Current_Value
            // 
            this.lbMotor_2_Current_Value.AutoSize = true;
            this.lbMotor_2_Current_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Current_Value.Location = new System.Drawing.Point(331, 83);
            this.lbMotor_2_Current_Value.Name = "lbMotor_2_Current_Value";
            this.lbMotor_2_Current_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_2_Current_Value.TabIndex = 7;
            this.lbMotor_2_Current_Value.Text = "Current";
            // 
            // lbMotor_2_Current_TimeStamp
            // 
            this.lbMotor_2_Current_TimeStamp.AutoSize = true;
            this.lbMotor_2_Current_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_2_Current_TimeStamp.Location = new System.Drawing.Point(111, 83);
            this.lbMotor_2_Current_TimeStamp.Name = "lbMotor_2_Current_TimeStamp";
            this.lbMotor_2_Current_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_2_Current_TimeStamp.TabIndex = 6;
            this.lbMotor_2_Current_TimeStamp.Text = "Time";
            // 
            // btSleep_Motor_1
            // 
            this.btSleep_Motor_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btSleep_Motor_1.Location = new System.Drawing.Point(498, 67);
            this.btSleep_Motor_1.Name = "btSleep_Motor_1";
            this.btSleep_Motor_1.Size = new System.Drawing.Size(110, 30);
            this.btSleep_Motor_1.TabIndex = 6;
            this.btSleep_Motor_1.Text = "Sleep Motor 1";
            this.btSleep_Motor_1.UseVisualStyleBackColor = false;
            this.btSleep_Motor_1.Click += new System.EventHandler(this.btSleep_Motor_1_Click);
            // 
            // btKill_Motor_1
            // 
            this.btKill_Motor_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btKill_Motor_1.Location = new System.Drawing.Point(498, 139);
            this.btKill_Motor_1.Name = "btKill_Motor_1";
            this.btKill_Motor_1.Size = new System.Drawing.Size(110, 30);
            this.btKill_Motor_1.TabIndex = 6;
            this.btKill_Motor_1.Text = "Kill Motor 1";
            this.btKill_Motor_1.UseVisualStyleBackColor = false;
            this.btKill_Motor_1.Click += new System.EventHandler(this.btKill_Motor_1_Click);
            // 
            // btResume_Motor_1
            // 
            this.btResume_Motor_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btResume_Motor_1.Location = new System.Drawing.Point(498, 103);
            this.btResume_Motor_1.Name = "btResume_Motor_1";
            this.btResume_Motor_1.Size = new System.Drawing.Size(110, 30);
            this.btResume_Motor_1.TabIndex = 6;
            this.btResume_Motor_1.Text = "Resume Motor 1";
            this.btResume_Motor_1.UseVisualStyleBackColor = false;
            this.btResume_Motor_1.Click += new System.EventHandler(this.btResume_Motor_1_Click);
            // 
            // btRun_Motor_1
            // 
            this.btRun_Motor_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btRun_Motor_1.Location = new System.Drawing.Point(498, 31);
            this.btRun_Motor_1.Name = "btRun_Motor_1";
            this.btRun_Motor_1.Size = new System.Drawing.Size(110, 30);
            this.btRun_Motor_1.TabIndex = 6;
            this.btRun_Motor_1.Text = "Run Motor 1";
            this.btRun_Motor_1.UseVisualStyleBackColor = false;
            this.btRun_Motor_1.Click += new System.EventHandler(this.btRun_Motor_1_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(498, 253);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 30);
            this.button1.TabIndex = 6;
            this.button1.Text = "Sleep Motor 2";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.btSleep_Motor_2_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button2.Location = new System.Drawing.Point(498, 217);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 30);
            this.button2.TabIndex = 6;
            this.button2.Text = "Run Motor 2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.btRun_Motor_2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button3.Location = new System.Drawing.Point(498, 289);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 30);
            this.button3.TabIndex = 6;
            this.button3.Text = "Resume Motor 2";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.btResume_Motor_2_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button4.Location = new System.Drawing.Point(498, 326);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(110, 30);
            this.button4.TabIndex = 6;
            this.button4.Text = "Kill Motor 2";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.btKill_Motor_2_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button5.Location = new System.Drawing.Point(498, 514);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(110, 30);
            this.button5.TabIndex = 8;
            this.button5.Text = "Kill Motor 3";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.button6.Location = new System.Drawing.Point(498, 477);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(110, 30);
            this.button6.TabIndex = 9;
            this.button6.Text = "Resume Motor 3";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button7.Location = new System.Drawing.Point(498, 405);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(110, 30);
            this.button7.TabIndex = 10;
            this.button7.Text = "Run Motor 3";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button8.Location = new System.Drawing.Point(498, 441);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(110, 30);
            this.button8.TabIndex = 11;
            this.button8.Text = "Sleep Motor 3";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbMotor_3_Speed_Value);
            this.groupBox3.Controls.Add(this.lbMotor_3_Voltage_Value);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.lbMotor_3_Voltage_TimeStamp);
            this.groupBox3.Controls.Add(this.lbMotor_3_Speed_TimeStamp);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.lbMotor_3_Current_Value);
            this.groupBox3.Controls.Add(this.lbMotor_3_Current_TimeStamp);
            this.groupBox3.Location = new System.Drawing.Point(12, 385);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(413, 168);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Motor3";
            // 
            // lbMotor_3_Speed_Value
            // 
            this.lbMotor_3_Speed_Value.AutoSize = true;
            this.lbMotor_3_Speed_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Speed_Value.Location = new System.Drawing.Point(331, 39);
            this.lbMotor_3_Speed_Value.Name = "lbMotor_3_Speed_Value";
            this.lbMotor_3_Speed_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_3_Speed_Value.TabIndex = 2;
            this.lbMotor_3_Speed_Value.Text = "Current";
            // 
            // lbMotor_3_Voltage_Value
            // 
            this.lbMotor_3_Voltage_Value.AutoSize = true;
            this.lbMotor_3_Voltage_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Voltage_Value.Location = new System.Drawing.Point(331, 126);
            this.lbMotor_3_Voltage_Value.Name = "lbMotor_3_Voltage_Value";
            this.lbMotor_3_Voltage_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_3_Voltage_Value.TabIndex = 11;
            this.lbMotor_3_Voltage_Value.Text = "Current";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(26, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Speed";
            // 
            // lbMotor_3_Voltage_TimeStamp
            // 
            this.lbMotor_3_Voltage_TimeStamp.AutoSize = true;
            this.lbMotor_3_Voltage_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Voltage_TimeStamp.Location = new System.Drawing.Point(111, 126);
            this.lbMotor_3_Voltage_TimeStamp.Name = "lbMotor_3_Voltage_TimeStamp";
            this.lbMotor_3_Voltage_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_3_Voltage_TimeStamp.TabIndex = 10;
            this.lbMotor_3_Voltage_TimeStamp.Text = "Time";
            // 
            // lbMotor_3_Speed_TimeStamp
            // 
            this.lbMotor_3_Speed_TimeStamp.AutoSize = true;
            this.lbMotor_3_Speed_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Speed_TimeStamp.Location = new System.Drawing.Point(111, 39);
            this.lbMotor_3_Speed_TimeStamp.Name = "lbMotor_3_Speed_TimeStamp";
            this.lbMotor_3_Speed_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_3_Speed_TimeStamp.TabIndex = 1;
            this.lbMotor_3_Speed_TimeStamp.Text = "Time";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(26, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Voltage";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(26, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "Current";
            // 
            // lbMotor_3_Current_Value
            // 
            this.lbMotor_3_Current_Value.AutoSize = true;
            this.lbMotor_3_Current_Value.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Current_Value.Location = new System.Drawing.Point(331, 83);
            this.lbMotor_3_Current_Value.Name = "lbMotor_3_Current_Value";
            this.lbMotor_3_Current_Value.Size = new System.Drawing.Size(57, 20);
            this.lbMotor_3_Current_Value.TabIndex = 7;
            this.lbMotor_3_Current_Value.Text = "Current";
            // 
            // lbMotor_3_Current_TimeStamp
            // 
            this.lbMotor_3_Current_TimeStamp.AutoSize = true;
            this.lbMotor_3_Current_TimeStamp.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbMotor_3_Current_TimeStamp.Location = new System.Drawing.Point(111, 83);
            this.lbMotor_3_Current_TimeStamp.Name = "lbMotor_3_Current_TimeStamp";
            this.lbMotor_3_Current_TimeStamp.Size = new System.Drawing.Size(42, 20);
            this.lbMotor_3_Current_TimeStamp.TabIndex = 6;
            this.lbMotor_3_Current_TimeStamp.Text = "Time";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 573);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btKill_Motor_1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btResume_Motor_1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btRun_Motor_1);
            this.Controls.Add(this.btSleep_Motor_1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Main";
            this.Text = "Main Page";
            this.Load += new System.EventHandler(this.Main_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Label lbMotor_1_Speed;
        private Label lbMotor_1_Speed_TimeStamp;
        private Label lbMotor_1_Speed_Value;
        private GroupBox groupBox1;
        private Label lbMotor_1_Voltage_Value;
        private Label lbMotor_1_Voltage_TimeStamp;
        private Label lbMotor_1_Voltage;
        private Label lbMotor_1_Current;
        private Label lbMotor_1_Current_Value;
        private Label lbMotor_1_Current_TimeStamp;
        private System.Windows.Forms.Timer UpdateTimer;
        private GroupBox groupBox2;
        private Label lbMotor_2_Speed_Value;
        private Label lbMotor_2_Voltage_Value;
        private Label lbMotor_2_Speed;
        private Label lbMotor_2_Voltage_TimeStamp;
        private Label lbMotor_2_Speed_TimeStamp;
        private Label lbMotor_2_Voltage;
        private Label lbMotor_2_Current;
        private Label lbMotor_2_Current_Value;
        private Label lbMotor_2_Current_TimeStamp;
        private Button btSleep_Motor_1;
        private Button btKill_Motor_1;
        private Button btResume_Motor_1;
        private Button btRun_Motor_1;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private GroupBox groupBox3;
        private Label lbMotor_3_Speed_Value;
        private Label lbMotor_3_Voltage_Value;
        private Label label3;
        private Label lbMotor_3_Voltage_TimeStamp;
        private Label lbMotor_3_Speed_TimeStamp;
        private Label label6;
        private Label label7;
        private Label lbMotor_3_Current_Value;
        private Label lbMotor_3_Current_TimeStamp;
    }
}
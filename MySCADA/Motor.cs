﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySCADA
{
    public class Motor
    {
        public string Name;
        public int Period;
        System.Timers.Timer UpdateTimer = null;
        System.Timers.Timer MonitorTimer = null;
        public List<Tag> Tags = new List<Tag>();
        public SCADA Parent;

        Random r = new Random();

        public Motor(string name, int period)
        {
            Name = name;
            Period = period;
        }

        public void AddTag(Tag tag){
            tag.Parent = this;
            Tags.Add(tag);
        }

        public Tag FindTag(string name)
        {
            foreach (Tag tag in Tags)
            {
                if (tag.Name == name)
                    return tag;
            }
            return null;
        }

        public void Engine()
        {
            if (UpdateTimer == null)
            {
                UpdateTimer = new System.Timers.Timer(Period);
                UpdateTimer.Elapsed += UpdateTags;
                UpdateTimer.Start();
            }
            /*
            if (MonitorTimer == null)
            {
                MonitorTimer = new System.Timers.Timer(Period);
                MonitorTimer.Elapsed += MonitorTags;
                MonitorTimer.Start();
            }
            */
            
        }
        public void Sleep()
        {
            if (UpdateTimer != null)
            {
                UpdateTimer.Stop();
            } else
            {
                string message = "Please run motor";
                MessageBox.Show(message);
            }
        }

        public void Resume()
        {
            if (UpdateTimer != null)
            {
                UpdateTimer.Start();
            } else
            {
                string message = "Please run motor";
                MessageBox.Show(message);
            }
        }

        public void Kill()
        {
            if (UpdateTimer != null)
            {
                UpdateTimer.Dispose();
                UpdateTimer = null;
            }
        }

        private void MonitorTags(object? sender, System.Timers.ElapsedEventArgs e)
        {
            Tag tag = null;
            for (int i = 0; i < Tags.Count; i++)
            {
                tag = Tags[i];
                Console.WriteLine($"Motor Name: {tag.Parent.Name} | Tag Name: {tag.Name} | TimeStamp = {tag.TimeStamp} | Value = {tag.Value}");
            }
        }

        private void UpdateTags(object? sender, System.Timers.ElapsedEventArgs e)
        {
            Tag tag = null;
            for  (int  i = 0; i < Tags.Count; i++)
            {
                tag = Tags[i];

                tag.Value = Parent.Devices[0].ReadTag($"{Name}.{tag.Name}");
                tag.TimeStamp = DateTime.Now;
            }
        }
    }
}

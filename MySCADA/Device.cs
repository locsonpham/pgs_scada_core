﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using S7.Net;

namespace MySCADA
{
    public class Device
    {
        public string IP;
        Plc thePLC = null;
        SCADA Parent;

        short Motor_1_Speed;
        float Motor_1_Current;
        float Motor_1_Voltage;

        short Motor_2_Speed;
        float Motor_2_Current;
        float Motor_2_Voltage;

        short Motor_3_Speed;
        float Motor_3_Current;
        float Motor_3_Voltage;

        System.Timers.Timer UpdateTimer = null;

        public Device(string ip)
        {
            IP = ip;

            thePLC = new Plc(CpuType.S71500, IP, 0, 1);

            try
            {
                thePLC.Open();
                Console.WriteLine($"the PLC {IP } is connected");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot connect to the PLC {IP}. {ex.Message}");
            }

            UpdateTimer = new System.Timers.Timer(500);
            UpdateTimer.Elapsed += UpdateTags;
            UpdateTimer.Start();
        }

        private void UpdateTags(object? sender, System.Timers.ElapsedEventArgs e)
        {
            if (thePLC.IsConnected)
            {
                Motor_1_Speed = Convert.ToInt16((ushort)thePLC.Read("MW100"));
                Motor_1_Current = ((uint)thePLC.Read("MD102")).ConvertToFloat();
                Motor_1_Voltage = ((uint)thePLC.Read("MD106")).ConvertToFloat();

                Motor_2_Speed = Convert.ToInt16((ushort)thePLC.Read("MW200"));
                Motor_2_Current = ((uint)thePLC.Read("MD202")).ConvertToFloat();
                Motor_2_Voltage = ((uint)thePLC.Read("MD206")).ConvertToFloat();

                Motor_3_Speed = Convert.ToInt16((ushort)thePLC.Read("MW300"));
                Motor_3_Current = ((uint)thePLC.Read("MD302")).ConvertToFloat();
                Motor_3_Voltage = ((uint)thePLC.Read("MD306")).ConvertToFloat();

                /*
                Console.WriteLine(Motor_1_Speed.ToString());
                Console.WriteLine(Motor_1_Current.ToString());
                Console.WriteLine(Motor_1_Voltage.ToString());
                Console.WriteLine("---");
                */
            }
        }

        public object ReadTag(string address)
        {
            object val = null;
            string[] temp = address.Split('.');

            switch (temp[0])
            {
                case "Motor_1":
                    {
                        switch (temp[1])
                        {
                            case "Speed":
                                {
                                    val = Motor_1_Speed;
                                    break;
                                }
                            case "Current":
                                {
                                    val = Motor_1_Current;
                                    break;
                                }
                            case "Voltage":
                                {
                                    val = Motor_1_Voltage;
                                    break;
                                }
                        }

                    }break;

                case "Motor_2":
                    {
                        switch (temp[1])
                        {
                            case "Speed":
                                {
                                    val = Motor_2_Speed;
                                    break;
                                }
                            case "Current":
                                {
                                    val = Motor_2_Current;
                                    break;
                                }
                            case "Voltage":
                                {
                                    val = Motor_2_Voltage;
                                    break;
                                }
                        }

                    }
                    break;

                case "Motor_3":
                    {
                        switch (temp[1])
                        {
                            case "Speed":
                                {
                                    val = Motor_3_Speed;
                                    break;
                                }
                            case "Current":
                                {
                                    val = Motor_3_Current;
                                    break;
                                }
                            case "Voltage":
                                {
                                    val = Motor_3_Voltage;
                                    break;
                                }
                        }

                    }
                    break;
            }

            return val;
        }
    }
}
